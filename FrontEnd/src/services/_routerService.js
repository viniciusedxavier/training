
import Dashboard from 'Routes/dashboard';
import Crm from 'Routes/crm';

import Users from 'Routes/users';


export default [
	{
		path: 'users',
		component: Users
	},
	{
		path: 'gestao',
		component: Dashboard
	},
	{
		path: 'crm',
		component: Crm
	},
]