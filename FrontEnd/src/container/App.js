/**
 * App.js Layout Start Here
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';

// rct theme provider
import RctThemeProvider from './RctThemeProvider';

//Horizontal Layout
import HorizontalLayout from './HorizontalLayout';

// async components
import {
	AsyncSessionLoginComponent,
	AsyncSessionRegisterComponent,
	AsyncSessionLockScreenComponent,
	AsyncSessionForgotPasswordComponent,
	AsyncSessionPage404Component,
	AsyncSessionPage500Component,
} from 'Components/AsyncComponent/AsyncComponent';

//Main App
import RctDefaultLayout from './DefaultLayout';

/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const InitialPath = ({ component: Component, ...rest }) =>
	<Route
		{...rest}
		render={props => <Component {...props} />}
	/>;

class App extends Component {
	render() {
		const { location, match, user } = this.props;
		if (location.pathname === '/') {
			if (user === null) {
				return (<Redirect to={'/session/login'} />);
			} else {
				return (<Redirect to={'/app/users/user-profile-1'} />);
			}
		}
		return (
			<RctThemeProvider>
				<NotificationContainer />
				<InitialPath
					path={`${match.url}app`}
					authUser={user}
					component={RctDefaultLayout}
				/>
				<Route path="/horizontal" component={HorizontalLayout} />
				{/* <Route path="/agency" component={AgencyLayout} /> */}
				{/* <Route path="/boxed" component={RctBoxedLayout} /> */}
				{/* <Route path="/dashboard" component={CRMLayout} /> */}
				{/* <Route path="/signin" component={AppSignIn} /> */}
				{/* <Route path="/signup" component={AppSignUp} /> */}
				<Route path="/session/login" component={AsyncSessionLoginComponent} />
				<Route path="/session/register" component={AsyncSessionRegisterComponent} />
				<Route path="/session/lock-screen" component={AsyncSessionLockScreenComponent} />
				<Route path="/session/forgot-password" component={AsyncSessionForgotPasswordComponent} />
				<Route path="/session/404" component={AsyncSessionPage404Component} />
				<Route path="/session/500" component={AsyncSessionPage500Component} />
				{/* <Route path="/terms-condition" component={AsyncTermsConditionComponent} /> */}
				{/* <Route path="/callback" render={(props) => { 
                handleAuthentication(props);
                return <Callback {...props} />
             }} />*/}
			</RctThemeProvider>
		);
	}
}



// map state to props
const mapStateToProps = ({ authUser }) => {
	const { user } = authUser;
	return { user };
};

export default connect(mapStateToProps)(App);
