import { NotificationManager } from 'react-notifications';
import {
	LOGIN_USER_SUCCESS,
	LOGIN_USER_FAILURE,
	LOGOUT_USER,
} from 'Actions/types';
import Axios from 'axios';

/**
 * Redux Action To Sigin User With Firebase
 */
export const signinUserInFirebase = (user, history) => (dispatch) => {
	Axios.post('http://localhost/Sense/teste/login', user)
		.then((response) => {
			sessionStorage.setItem("user_id", response.data);
			dispatch({ type: LOGIN_USER_SUCCESS, payload: sessionStorage.getItem('user_id') });
			history.push('/');
			//  NotificationManager.success('User Login Successfully!');
		})
		.catch((error) => {
			dispatch({ type: LOGIN_USER_FAILURE });
			NotificationManager.error(error.message);
		});
}

/**
 * Redux Action To Signout User From  Firebase
 */
export const logoutUserFromFirebase = () => {
	sessionStorage.removeItem('user_id');
	history.push('/');
	// NotificationManager.success('User Logout Successfully');
}
