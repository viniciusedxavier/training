module.exports = {
	"sidebar.gestao": "Gestão",
	"sidebar.usuarios": "Usuários",
	"sidebar.app": "TrainingPlus",

	"sidebar.gettingStarted": " ",
	"sidebar.aboutUs": "Sobre a SensePlus",
	"sidebar.faq(s)": "Perguntas Frequentes",
	"sidebar.terms&Conditions": "Termos e Condições",
	"sidebar.feedback": "Feedback",
	"sidebar.general": "Geral",
	"sidebar.users": "Usuários",
	"sidebar.userProfile": "Perfil",
	"widgets.messages": "Mensagens",
	"widgets.logOut": "Sair",
	"widgets.profile": "Perfil",

	"components.myProfile": "Meu Perfil",
	"components.emailPrefrences": "Preferências de Email",
	"widgets.personalDetails": "Detalhes",
	"components.nome": "Nome",
	"components.login": "Login",
	"components.email": "Email",
	"components.sexo": "Sexo",
	"widgets.updateProfile": "Atualizar",

}