import appLocaleData from 'react-intl/locale-data/pt';
import brMessages from '../locales/pt_BR';

const brLang = {
	messages: {
		...brMessages
	},
	locale: 'pt-BR',
	data: appLocaleData
};
export default brLang;