/**
 * Confirmation Dialog
 */
import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { InputGroup } from 'reactstrap';
import { Checkbox } from '@material-ui/core';

class ConfirmationDialog extends React.Component {
	state = {
	};

	UNSAFE_componentWillMount() {
	}

	UNSAFE_componentWillUpdate() {
	}


	handleCancel = () => {
		this.props.onCancel();
	};

	handleOk = () => {
		this.props.onClose(this.state);
	};

	handleChange = (event) => {
		this.setState({ [event.target.id]: !this.state[event.target.id] });
	};

	handleEntering = () => {
		this.setState({ ...this.props.options });
	}

	render() {
		const { options, ...other } = this.props;

		return (
			<Dialog maxWidth="sm" onEntering={this.handleEntering}
				aria-labelledby="confirmation-dialog-title"
				{...other}>
				<DialogTitle id="confirmation-dialog-title">Papéis</DialogTitle>
				<DialogContent>
					<InputGroup
						aria-label="ringtone"
						name="ringtone">
						{Object.keys(options).map(option => (
							<FormControlLabel value={option} key={option} control={<Checkbox id={option} checked={!!this.state[option]} onChange={this.handleChange} />} label={option} />
						))}
					</InputGroup>
				</DialogContent>
				<DialogActions>
					<Button variant="contained" size="small" onClick={this.handleCancel} className="btn-danger text-white">
						Cancel
          </Button>
					<Button variant="contained" size="small" onClick={this.handleOk} color="primary" className="text-white">
						Ok
          </Button>
				</DialogActions>
			</Dialog>
		);
	}
}

ConfirmationDialog.propTypes = {
	onClose: PropTypes.func,
};

export default ConfirmationDialog;