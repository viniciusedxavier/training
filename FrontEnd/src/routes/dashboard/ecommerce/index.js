/**
 * Ecommerce Dashboard
 */

import React, { Component, Fragment } from 'react';
import {
	Button,
	Form,
	FormGroup,
	Label,
	Input,
	FormText,
	Col,
	FormFeedback,
} from 'reactstrap';
import ConfirmationDialog from '../../dialog/ConfirmDialog';

import MatButton from '@material-ui/core/Button';

import Axios from 'axios';
import IconButton from '@material-ui/core/IconButton';

import { Badge } from 'reactstrap';

import MUIDataTable from "mui-datatables";
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// rct card box

import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
let papeis;
papeis = [
	{
		"id": 2,
		"nome": "Administrador"
	},
	{
		"id": 3,
		"nome": "Instrutor"
	},
	{
		"id": 4,
		"nome": "Aluno"
	}
];
const columns = [{ name: "nome", label: "Nome" }, { name: "nivel", label: "Papéis" }, { name: "matricula", label: "Matrícula" }, { name: "ativo", label: "Situação" }];
const data = [];
const options = {
	textLabels: {
		body: {
			noMatch: "Não há registros para exibir!",
			toolTip: "Ordenar",
			columnHeaderTooltip: column => `Ordenar por ${column.label}`
		},
		pagination: {
			next: "Próximo",
			previous: "Anterior",
			rowsPerPage: "Linhas por página:",
			displayRows: "de",
		},
		toolbar: {
			search: "Buscar",
			downloadCsv: "Download CSV",
			print: "Imprimir",
			viewColumns: "Esconder/Mostrar Colunas",
			filterTable: "Filtrar",
		},
		filter: {
			all: "Todos",
			title: "FILTROS",
			reset: "LIMPAR",
		},
		viewColumns: {
			title: "Mostrar colunas",
			titleAria: "Exibir/Ocultar colunas",
		},
		selectedRows: {
			text: "linha(s) selecionadas",
			delete: "Deletar",
			deleteAria: "Deletar Linhas Selecionadas",
		},
	}
};

export default class EcommerceDashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: { papeis: [] }
			, open: false
		}
		Axios.get('http://localhost/Sense/teste/permissao/master',
			{ headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('user_id') } }).then(() => papeis.unshift({ "id": 1, "nome": "Master" }))
	}

	openModal = (data) => {
		this.setState({ value: data }, () => this.setState({ open: true }));
	}

	onCancel = () => {
		this.setState({ open: false })
	}

	onClose = (novosPapeis) => {
		let usuario = this.state.value;
		usuario.papeis = papeis.reduce((acc, cur) => {
			console.log(acc)
			if (novosPapeis[cur.nome]) acc[acc.length] = cur;
			return acc;
		}, [])
		Axios.post('http://localhost/Sense/teste/papel/alterar', usuario, { headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('user_id') } }).then(() => {
			this.componentDidMount();
			this.setState({ open: false });
		})
	}

	componentDidMount() {
		Axios.get('http://localhost/Sense/teste/usuarios',
			{ headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('user_id') } }
		).then(response => this.setState({
			usuarios:
				response.data.map(data => ({
					...data,
					acoes: <>
						<IconButton className="text-success" aria-label="Delete"><i className="zmdi zmdi-check-all"></i></IconButton>
						<IconButton className="text-danger" aria-label="Add an alarm"><i className="zmdi zmdi-close"></i></IconButton></>,
					nivel: <MatButton onClick={() => this.openModal(data)} className="text-secondary mr-10 mb-10">{data.papeis.map(papel => papel.nome).join(', ')}</MatButton>,
					ativo: data.ativo ? <Badge color="success">Ativo</Badge> : <Badge color="warning">Inativo</Badge>,
				}))
		}))
	}

	render() {

		const { match } = this.props;

		return (
			<div className="ecom-dashboard-wrapper">
				<PageTitleBar title={<IntlMessages id="sidebar.usuarios" />} match={match} />
				<div className="row">
					<div className="col-sm-12 col-md-12 col-xl-12">
						<MUIDataTable
							title={"Usuários"}
							data={this.state.usuarios}
							columns={columns}
							options={options}
						/>
					</div>
				</div>

				<ConfirmationDialog
					open={this.state.open}
					onClose={this.onClose}
					onCancel={this.onCancel}
					options={
						papeis.reduce((obj, papel) => {
							obj[papel.nome] = this.state.value.papeis.find(valor => valor.nome === papel.nome)
							return obj;
						}, {})
					}
				/>
			</div>
		)
	}
}
