/**
 * Profile Page
 */
import React, { Component } from 'react';
import { FormGroup, Input, Form, Label, Col, InputGroup, InputGroupAddon } from 'reactstrap';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { NotificationManager } from 'react-notifications';

import Axios from 'axios';

// intlmessages
import IntlMessages from 'Util/IntlMessages';

export default class Profile extends Component {

	componentDidUpdate() {
		if (Object.keys(this.state).length !== Object.keys(this.props.entity).length) {
			this.setState({ ...this.props.entity })
		}
	}
	state = {}


	/**
	 * On Update Profile
	 */
	onUpdateProfile() {
		Axios.post('http://localhost/Sense/teste/usuario/alterar', this.state,
			{ headers: { 'Authorization': 'Bearer ' + sessionStorage.getItem('user_id') } }
		).then(() => NotificationManager.success('Perfil Atualizado!'))

	}

	onChange = e => this.setState({ [e.target.id]: e.target.value });

	render() {
		return (
			<div className="profile-wrapper w-50">
				<h2 className="heading"><IntlMessages id="widgets.personalDetails" /></h2>
				<Form>
					<FormGroup row>
						<Label for="nome" sm={3}><IntlMessages id="components.nome" /></Label>
						<Col sm={9}>
							<Input onChange={this.onChange} value={this.state.nome} type="text" name="nome" id="nome" className="input-lg" />
						</Col>
					</FormGroup>
					<FormGroup row>
						<Label for="login" sm={3}><IntlMessages id="components.login" /></Label>
						<Col sm={9}>
							<Input onChange={this.onChange} value={this.state.login} type="text" name="login" id="login" className="input-lg" />
						</Col>
					</FormGroup>
					<FormGroup row>
						<Label for="email" sm={3}><IntlMessages id="components.email" /></Label>
						<Col sm={9}>
							<Input onChange={this.onChange} value={this.state.email} type="text" name="email" id="email" className="input-lg" />
						</Col>
					</FormGroup>
					<FormGroup row>
						<Label for="sexo" sm={3}><IntlMessages id="components.sexo" /></Label>
						<Col sm={9}>
							<Input onChange={this.onChange} value={this.state.sexo} type="text" name="sexo" id="sexo" className="input-lg mb-20" />
						</Col>
					</FormGroup>
				</Form>
				<Button variant="contained" color="primary" className="text-white" onClick={() => this.onUpdateProfile()}><IntlMessages id="widgets.updateProfile" /></Button>
			</div>
		);
	}
}
