// sidebar nav links
export default {
	category1: [
		{
			"menu_title": "sidebar.gestao",
			"menu_icon": "zmdi zmdi-view-dashboard",
			"new_item": false,
			"type_multi": null,
			"niveis": ["master", "admin"],
			"child_routes": [
				{
					"menu_title": "sidebar.usuarios",
					"new_item": false,
					"path": "/app/gestao/usuarios",
				},
			]
		}
	]
}
