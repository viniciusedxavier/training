package com.senseplus.dao;

import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;

/**
 * Implementacao do <code>MyBatisDAO</code> que adiciona metodos para operacao
 * em Batch com e sem controle de transacao</br>
 * 
 * @param <T> A entidade a ser persistida pelo DAO
 * @see MyBatisDAO
 */
public abstract class BatchDAO<T> extends MyBatisDAO<T> {

	/**
	 * Define prefixes for easier naming convetions between XML mapper files and the
	 * DAO class
	 **/
	protected static final String PREFIX_INSERT_BATCH_QUERY = PREFIX_INSERT_QUERY + "Batch";
	protected static final String PREFIX_UPDATE_BATCH_QUERY = PREFIX_UPDATE_QUERY + "Batch";
	protected static final String PREFIX_DELETE_BATCH_QUERY = PREFIX_DELETE_QUERY + "Batch";

	public BatchDAO(Class<T> type) {
		super(type);
	}

	/**
	 * Insere os objetos de um <code>List&lt;T&gt;</code> no banco de dados.
	 * 
	 * @param list A lista de objetos a serem inseridos no banco de dados.
	 * @return A quantidade de linhas afetadas pela operacao.
	 * @throws PersistenceException
	 */
	public int insertBatch(List<T> list) throws PersistenceException {

		SqlSession session = sf.openSession();
		Integer status = null;
		try {
			String query = NAMESPACE + "." + PREFIX_INSERT_BATCH_QUERY + list.get(0).getClass().getSimpleName();
			status = (Integer) session.insert(query, list);
			session.commit();
		} finally {
			session.close();
		}
		return status;

	}

	/**
	 * Insere os objetos de um <code>List&lt;T&gt;</code> no banco de dados.</br>
	 * <b>Observacao:</b> Este metodo nao efetua(commit) as SQLs executadas na
	 * sessao, apenas executa deixando para ser efetuadas(commit) por quem estah
	 * controlando a transacao.
	 * 
	 * @param list    A lista de objetos a serem inseridos no banco de dados.
	 * @param session A sessao onde as SQLs serao executada.
	 * @return A quantidade de linhas afetadas pela operacao(apos commit)
	 * @throws PersistenceException
	 */
	public int insertBatch(List<T> list, SqlSession session) throws PersistenceException {

		// se a lista estah vazia, nada a inserir
		if ((list == null) || (list.size() == 0))
			return 0;

		Integer status = null;

		String query = NAMESPACE + "." + PREFIX_INSERT_BATCH_QUERY + list.get(0).getClass().getSimpleName();
		status = (Integer) session.insert(query, list);

		return status;

	}

	/**
	 * Atualiza os objetos de um <code>List&lt;T&gt;</code> no banco de dados.
	 * 
	 * @param list A lista de objetos a serem atualizados no banco de dados.
	 * @return O numero de registros atualizados.
	 * @throws PersistenceException
	 */
	public int updateBatch(List<T> list) throws PersistenceException {

		SqlSession session = sf.openSession();
		Integer status = null;
		try {
			String query = NAMESPACE + "." + PREFIX_UPDATE_BATCH_QUERY + type.getClass().getSimpleName();
			status = session.update(query, list);
			session.commit();

		} finally {
			session.close();
		}
		return status;
	}

	/**
	 * Atualiza os objetos de um <code>List&lt;T&gt;</code> no banco de dados.</br>
	 * <b>Observacao:</b> Este metodo nao efetua(commit) as SQLs executadas na
	 * sessao, apenas executa deixando para ser efetuadas(commit) por quem estah
	 * controlando a transacao.
	 * 
	 * @param list    A lista de objetos a serem atualizados no banco de dados.
	 * @param session A sessao onde as SQLs serao executada.
	 * @return o numero de registros que serao atualizados(apos commit)
	 * @throws PersistenceException
	 */
	public int updateBatch(List<T> list, SqlSession session, Class<?> clazz) throws PersistenceException {
		// se a lista estah vazia, nada a inserir
		if ((list == null) || (list.size() == 0))
			return 0;

		Integer status = null;

		String query = NAMESPACE + "." + PREFIX_UPDATE_BATCH_QUERY + list.get(0).getClass().getSimpleName();
		status = (Integer) session.update(query, list);

		return status;
	}

	/**
	 * Apaga no banco de dados os objetos de um <code>List&lt;T&gt;</code>.
	 * 
	 * @param list A lista de objetos a serem apagados no banco de dados.
	 * @return O numero de registros apagados.
	 * @throws PersistenceException
	 */
	public int deleteBatch(List<T> list) throws PersistenceException {

		SqlSession session = sf.openSession();
		Integer status = null;
		try {
			String query = NAMESPACE + "." + PREFIX_DELETE_BATCH_QUERY + this.type.getSimpleName();
			status = session.delete(query, list);
			session.commit();
		} finally {
			session.close();
		}
		return status;

	}

	/**
	 * Apaga no banco de dados os objetos de um <code>List&lt;T&gt;</code>.</br>
	 * <b>Observacao:</b> Este metodo nao efetua(commit) as SQLs executadas na
	 * sessao, apenas executa deixando para ser efetuadas(commit) por quem estah
	 * controlando a transacao.
	 * 
	 * @param list    A lista de objetos a serem apagados no banco de dados.
	 * @param session A sessao onde as SQLs serao executada.
	 * @return o numero de registros apagados
	 * @throws PersistenceException
	 */
	public int deleteBatch(int id, SqlSession session) throws PersistenceException {

		Integer status = null;

		String query = NAMESPACE + "." + PREFIX_DELETE_BATCH_QUERY + this.type.getSimpleName();
		status = session.delete(query, id);

		return status;

	}
}
