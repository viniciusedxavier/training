package com.senseplus.dao;

import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;

/**
 * Interface que define os metodos CRUD de todos os DAOs.
 * 
 * @param <T> A entidade a ser persistida pelo DAO.
 */
public interface BaseDAO<T> {

	/**
	 * Insere um registro no banco de dados.</br>
	 * <b>Observacao:</b> Este metodo atualiza o campo ID do objeto com o valor do
	 * ID que o banco gerou. Assim eh possivel pegar o ID deste objeto atualizar
	 * objetos(FKs) que fazem referencia a ele.
	 * 
	 * @param object Um objeto mapeado para um tabela no banco(ver classes em:
	 *               <code>com.sinapsis.prediz.persistence.entities</code>)
	 * @return A quantidade de linhas afetadas pela operacao - 1 se o registro foi
	 *         inserido.
	 * @throws PersistenceException
	 */
	public int insert(T object) throws PersistenceException;
	
	/**
	 * Insere um registro no banco de dados.</br>
	 * <b>Observacao:</b> Este metodo atualiza o campo ID do objeto com o valor do
	 * ID que o banco gerou. Assim eh possivel pegar o ID deste objeto atualizar
	 * objetos(FKs) que fazem referencia a ele.
	 * Utilize esta sobrecarga para executar varios inserts em uma so sessao.
	 * 
	 * @param object Um objeto mapeado para um tabela no banco(ver classes em:
	 *               <code>com.sinapsis.prediz.persistence.entities</code>)
	 * @return A quantidade de linhas afetadas pela operacao - 1 se o registro foi
	 *         inserido.
	 * @throws PersistenceException
	 */
	public int insert(T object, SqlSession session) throws PersistenceException;

	/**
	 * Atualiza um registro no banco de dados.
	 * 
	 * @param object Um objeto mapeado para um tabela no banco(ver classes em:
	 *               <code>com.sinapsis.prediz.persistence.entities</code>)
	 * @return A quantidade de linhas afetadas pela operacao - 1 se o registro foi
	 *         atualizado.
	 * @throws PersistenceException
	 */
	public int update(T object) throws PersistenceException;

	/**
	 * Atualiza um registro no banco de dados.
	 * Utilize esta sobrecarga para executar varios updates em uma so sessao.
	 * @param object Um objeto mapeado para um tabela no banco(ver classes em:
	 *               <code>com.sinapsis.prediz.persistence.entities</code>)
	 * @return A quantidade de linhas afetadas pela operacao - 1 se o registro foi
	 *         atualizado.
	 * @throws PersistenceException
	 */
	public int update(T object, SqlSession session) throws PersistenceException;
	
	/**
	 * Exclui um registro do banco de dados.
	 * 
	 * @param id O ID do registro.
	 * @return A quantidade de linhas afetadas pela operacao - 1 se o registro foi
	 *         excluido.
	 * @throws PersistenceException
	 */
	public int delete(Integer id) throws PersistenceException;
	
	/**
	 * Exclui um registro do banco de dados.
	 * Utilize esta sobrecarga para executar varios deletes em uma so sessao.
	 * 
	 * @param id O ID do registro.
	 * @return A quantidade de linhas afetadas pela operacao - 1 se o registro foi
	 *         excluido.
	 * @throws PersistenceException
	 */
	public int delete(Integer id, SqlSession session) throws PersistenceException;

	/**
	 * Seleciona um registro pela primary key - (ID).</br>
	 * Este metodo <b>NAO</b> carrega todos os campos de todos os objetos contidos
	 * no objeto principal '<code>T</code>', carrega somente os IDs destes, para
	 * carregar tudo utilize o metodo <code>loadFull</code>.
	 * 
	 * @param id O ID do registro.
	 * @return Um objeto do tipo <code>T</code> populado com as informacoes do
	 *         registro selecionado.
	 * @throws PersistenceException
	 * @see {@link #loadFull(int) loadFull}
	 */
	public T get(Integer id) throws PersistenceException;

	/**
	 * Seleciona todos os registros do banco. </br>
	 * SQL Executada: select * from [tablename] </br>
	 * </br>
	 * <b>Notes:</b></br>
	 * Consider overdiding this method in order to handle large numbers of objects
	 * with multiple references. LAZY LOADING should be enabled in this case,
	 * otherwise you might run out of memory (eg. get all UserAccounts if the table
	 * has 1,000,000 rows) look into the aggresiveLazyLoading property
	 * 
	 * @return Um <code>List</code> de objetos do tipo <code>T</code>.
	 * @throws PersistenceException
	 */
	public List<T> getAll() throws PersistenceException;

	/**
	 * Seleciona o primerio registro pelo campo descritivo, geralmente NOME, CODIGO
	 * ou DESCRICAO.</br>
	 * SQL Executed (example): select * from [tablename] where NAME = ?</br>
	 * </br>
	 * It's up to you to decide what constitutes an object's name. Typically you
	 * would have a NAME column in the table, but not all objects have this.
	 * Generally this method should be overriden (if you need it at all) in the
	 * child dao class. </br>
	 * </br>
	 * <b>Observacao:</b> Verifique se existe indice no bando de dados para o campo
	 * utilizado na consulta! </br>
	 * </br>
	 * 
	 * @param name O valor do campo NOME, CODIGO ou DESCRICAO do registro.
	 * @return Um objeto do tipo <code>T</code> populado com as informacoes do
	 *         registro selecionado.
	 * @throws PersistenceException
	 */
	public T getByName(String name) throws PersistenceException;

	/** 
	 * Seleciona um registro pela primary key - (ID).</br>
	 * Este metodo carrega tambem todos os campos de todos os objetos contidos no
	 * objeto principal '<code>T</code>', enquanto o metodo <code>get</code> carrega
	 * somente os IDs destes objetos.
	 * 
	 * @param id O ID do registro.
	 * @return Um objeto do tipo <code>T</code> - e os objetos contidos nele -
	 *         populados com as informacoes dos registros selecionados.
	 * @throws PersistenceException
	 * @see {@link #get(int) get}
	 */
	public T loadFull(Integer id) throws PersistenceException;
	
	/** 
	 * Seleciona uma lista de registros por uma foreign key - (FK).</br>
	 * 
	 * @param id O ID do registro.
	 * @return Uma lista de objetos filtrado por uma foreign key em comum
	 * @throws PersistenceException
	 * @see {@link #get(int) get}
	 */
	public List<T> findAll(Integer id) throws PersistenceException;

}