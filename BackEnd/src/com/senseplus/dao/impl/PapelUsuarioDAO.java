package com.senseplus.dao.impl;

import com.senseplus.dao.BatchDAO;
import com.senseplus.persistence.entities.PapelUsuario;

public class PapelUsuarioDAO extends BatchDAO<PapelUsuario> {
	
	public PapelUsuarioDAO(Class<PapelUsuario> type) {
		super(type);
	}

}
