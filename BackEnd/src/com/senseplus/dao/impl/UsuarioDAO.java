package com.senseplus.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.ibatis.session.SqlSession;

import com.senseplus.dao.DAO;
import com.senseplus.dao.MyBatisDAO;
import com.senseplus.persistence.entities.PapelUsuario;
import com.senseplus.persistence.entities.Usuario;

public class UsuarioDAO extends MyBatisDAO<Usuario> {
	
	public UsuarioDAO(Class<Usuario> type) {
		super(type);
	}

	public Usuario auth(String login) {

		SqlSession session = sf.openSession();
		Usuario obj = null;
		try {
			String query = NAMESPACE + "." + "auth";
			obj = session.selectOne(query, login);
		} finally {
			session.close();
		}
		return obj;
	}

	public int insert(Usuario usuario) {
		super.insert(usuario);
		List<PapelUsuario> list = usuario.getPapeis().stream().map(papel->new PapelUsuario(papel, usuario)).collect(Collectors.toList());
		DAO.PAPEL_USUARIO.insertBatch(list);		
		
		return 0;
	}
}
