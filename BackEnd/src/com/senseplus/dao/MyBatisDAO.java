package com.senseplus.dao;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;


/**
 * Implementacao utilizando MyBatis do <code>BaseDAO</code>.
 * 
 * @param <T> A entidade a ser persistida pelo DAO.
 * @see BaseDAO
 */
@SuppressWarnings("unchecked")
public abstract class MyBatisDAO<T> implements BaseDAO<T> {

	//
	// atributos
	//
	protected static final String NAMESPACE = "mappers";
	protected static SqlSessionFactory sf;
	protected Class<T> type;

	/**
	 * Define prefixes for easier naming convetions between XML mapper files and the
	 * DAO class
	 **/
	protected static final String PREFIX_INSERT_QUERY = "insert";
	protected static final String PREFIX_UPDATE_QUERY = "update";
	protected static final String PREFIX_DELETE_QUERY = "delete";
	protected static final String PREFIX_SELECT_QUERY = "get";
	protected static final String PREFIX_SELECT_ALL_QUERY = "getAll";
	protected static final String PREFIX_LOAD_FULL_QUERY = "loadFull";
	protected static final String PREFIX_FIND_ALL_QUERY = "findAll";

	// mecanismo de log do DAO
	private static Logger log = Logger.getLogger(MyBatisDAO.class.getName());

	//
	// Construtores
	//

	/**
	 * Construtor utilizado pelas implementacao.</br>
	 * Responsavel por garantir uma unica instancia do SqlSessionFactory.
	 * 
	 * @param type A classe da entidade a ser persistida pelo DAO.
	 * @see SqlSessionFactory
	 */
	public MyBatisDAO(Class<T> type) {

		this.type = type;

		//
		// garante uma unica instancia do SqlSessionFactory
		//
		if (sf == null) {
			try {

				sf = PersistenceFactory.getInstance();

			} catch (IOException e) {
				log.severe("Could not instantiate MyBatisDAO. Loading myBatis sessionFactory failed.");
				e.printStackTrace();
				System.exit(1);
			}
		}

	}

	//
	// Implementacao dos metodos definidos na interface e metodos base.
	//

	/**
	 * Retorna factory de secao. Cada SQL(ou conjunto de SQLs para transacao) devem
	 * ser realizada uma secao fornecida pelo SqlSessionFactory.
	 * 
	 * @return O objeto do tipo <code>SqlSessionFactory</code>
	 * @see SqlSessionFactory
	 */
	protected SqlSessionFactory getSessionFactory() {
		return sf;
	}

	@Override
	public int insert(T o) throws PersistenceException {
		SqlSession session = sf.openSession();
		Integer status = null;
		try {
			String query = NAMESPACE + "." + PREFIX_INSERT_QUERY + o.getClass().getSimpleName();
			status = (Integer) session.insert(query, o);
			session.commit();
		} finally {
			session.close();
		}
		return status;
	}
	
	@Override
	public int insert(T o, SqlSession session) throws PersistenceException {
		Integer status = null;
		String query = NAMESPACE + "." + PREFIX_INSERT_QUERY + o.getClass().getSimpleName();
		status = (Integer) session.insert(query, o);

		return status;
	}


	@Override
	public int update(T o) throws PersistenceException {

		String query = NAMESPACE + "." + PREFIX_UPDATE_QUERY + o.getClass().getSimpleName();
		return update(query, o);

	}

	@Override
	public int update(T o, SqlSession session) throws PersistenceException {
		Integer status = null;
		String query = NAMESPACE + "." + PREFIX_UPDATE_QUERY + o.getClass().getSimpleName();
		status = (Integer) session.update(query, o);

		return status;
	}

	protected int update(String query, T o) throws PersistenceException {

		SqlSession session = sf.openSession();
		Integer status = null;
		try {
			status = session.update(query, o);
			session.commit();

		} finally {
			session.close();
		}
		return status;

	}

	@Override
	public int delete(Integer id) throws PersistenceException {

		String query = NAMESPACE + "." + PREFIX_DELETE_QUERY + this.type.getSimpleName();
		return delete(query, id);

	}

	@Override
	public int delete(Integer i, SqlSession session) throws PersistenceException {
		Integer status = null;
		String query = NAMESPACE + "." + PREFIX_DELETE_QUERY + this.type.getSimpleName();
		status = (Integer) session.delete(query, i);

		return status;
	}
	
	protected int delete(String query, Integer id) throws PersistenceException {

				
		SqlSession session = sf.openSession();
		Integer status = null;
		try {
			status = session.delete(query, id);
			
			session.commit();
		} finally {
			session.close();
		}
		return status;

	}
	

	@Override
	public T get(Integer id) throws PersistenceException {

		String query = NAMESPACE + "." + PREFIX_SELECT_QUERY + this.type.getSimpleName();
		return get(query, id);
	}

	protected T get(String query, Integer id) throws PersistenceException {

		SqlSession session = sf.openSession();
		T obj = null;
		try {
			obj = (T) session.selectOne(query, id);
		} finally {
			session.close();
		}
		return obj;
	}

	@Override
	public List<T> getAll() throws PersistenceException {

		SqlSession session = sf.openSession();
		List<T> list = null;
		try {
			String query = NAMESPACE + "." + PREFIX_SELECT_ALL_QUERY + this.type.getSimpleName();
			list = (List<T>) session.selectList(query);
		} finally {
			session.close();
		}
		return list;
	}

	@Override
	public T getByName(String name) throws PersistenceException {

		SqlSession session = sf.openSession();
		T obj = null;
		try {
			String query = NAMESPACE + "." + PREFIX_SELECT_QUERY + this.type.getSimpleName() + "ByName";
			obj = (T) session.selectOne(query, name);
		} finally {
			session.close();
		}
		return obj;
	}

	@Override
	public T loadFull(Integer id) throws PersistenceException {
		String query = NAMESPACE + "." + PREFIX_LOAD_FULL_QUERY + this.type.getSimpleName();
		return get(query, id);
	}
	

	@Override
	public List<T> findAll(Integer id) throws PersistenceException {

		SqlSession session = sf.openSession();
		List<T> list = null;
		try {
			String query = NAMESPACE + "." + PREFIX_FIND_ALL_QUERY + this.type.getSimpleName();
			list = (List<T>) session.selectList(query, id);
		} finally {
			session.close();
		}
		return list;
	}


}