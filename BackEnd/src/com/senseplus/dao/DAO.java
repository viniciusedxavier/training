package com.senseplus.dao;

import com.senseplus.dao.impl.PapelUsuarioDAO;
import com.senseplus.dao.impl.UsuarioDAO;
import com.senseplus.persistence.entities.PapelUsuario;
import com.senseplus.persistence.entities.Usuario;

/**
 * Single Point of Access - Data Access Object
 */
public class DAO {

	 public static UsuarioDAO USUARIO = new UsuarioDAO(Usuario.class);
	 public static PapelUsuarioDAO PAPEL_USUARIO = new PapelUsuarioDAO(PapelUsuario.class);
	 
}
