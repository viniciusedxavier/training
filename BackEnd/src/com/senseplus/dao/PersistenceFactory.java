package com.senseplus.dao;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class PersistenceFactory {

	private static SqlSessionFactory instance;
	

	private PersistenceFactory() {
	}

	public static SqlSessionFactory getInstance() throws IOException {
		if (instance == null) {
			String resource = "com/senseplus/dao/impl/mybatis-config.xml";
			InputStream inputStream = Resources.getResourceAsStream(resource);
			instance =  new SqlSessionFactoryBuilder().build(inputStream);
		}
		return instance;
	}

}
