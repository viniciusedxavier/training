package com.senseplus.persistence.entities;

import java.util.List;

public class Papel {
	
	@Override
	public String toString() {
		return "Papel [id=" + id + ", nome=" + nome + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Papel other = (Papel) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}
	private Integer id;
	private String nome;
	private List<Operacao> permissoes;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<Operacao> getPermissoes() {
		return permissoes;
	}
	public void setPermissoes(List<Operacao> permissoes) {
		this.permissoes = permissoes;
	}
}
