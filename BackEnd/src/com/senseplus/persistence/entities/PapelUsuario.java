package com.senseplus.persistence.entities;

public class PapelUsuario {
	private Papel papel;
	private Usuario usuario;
	
	
	public Papel getPapel() {
		return papel;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public PapelUsuario(Papel papel, Usuario usuario) {
		super();
		this.papel = papel;
		this.usuario = usuario;
	}
}
