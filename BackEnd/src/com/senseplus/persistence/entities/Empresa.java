package com.senseplus.persistence.entities;

public class Empresa {
	private Integer id;
	private String razaoSocial;
	private String cnpj;
	private int qtdLicenca;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public int getQtdLicenca() {
		return qtdLicenca;
	}
	public void setQtdLicenca(int qtdLicenca) {
		this.qtdLicenca = qtdLicenca;
	}
	
	
}
