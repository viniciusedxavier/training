package com.senseplus.persistence.entities;

import java.util.Date;
import java.util.List;

public class Usuario {
	@Override
	public String toString() {
		return "Usuario [papeis=" + papeis + "]";
	}
	private Integer id;
	private String nome;
	private String email;
	private String login;
	private String senha;
	private String sexo;
	private Integer matricula;
	private boolean ativo;
	private Date dataCadastro;
	private Usuario usuarioUltimoAlteracao;
	private List<Papel> papeis;
	private Empresa empresa;
	
	public Empresa getEmpresa() {
		return empresa;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public List<Papel> getPapeis() {
		return papeis;
	}
	public void setPapeis(List<Papel> papeis) {
		this.papeis = papeis;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Usuario getUsuarioUltimoAlteracao() {
		return usuarioUltimoAlteracao;
	}
	public void setUsuarioUltimoAlteracao(Usuario usuarioUltimoAlteracao) {
		this.usuarioUltimoAlteracao = usuarioUltimoAlteracao;
	}
}
