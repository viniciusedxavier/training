package com.senseplus.security;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswordEncryptor {
	public static void main(String[] args) {
		System.out.println(generateStrongPasswordHash("alexandra"));
	}

	public static String generateStrongPasswordHash(String password){
		int iterations = 1000;
		char[] chars = password.toCharArray();
		byte[] salt = getSalt();

		PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
		Arrays.fill(chars, Character.MIN_VALUE);
		SecretKeyFactory skf;
		try {
			skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
			byte[] hash = skf.generateSecret(spec).getEncoded();
			return iterations + ":" + toHex(salt) + ":" + toHex(hash);
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Algoritmo n�o encontrado!");
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			System.err.println("Erro no processo de Hash!");
			e.printStackTrace();
		}finally {
			spec.clearPassword();
		}
		return "";
	}

	public static byte[] getSalt(){
		SecureRandom sr = new SecureRandom();	
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt;
	}

	public static String toHex(byte[] array) throws NoSuchAlgorithmException {
		BigInteger bi = new BigInteger(1, array);
		String hex = bi.toString(16);
		int paddingLength = (array.length * 2) - hex.length();
		if (paddingLength > 0) {
			return String.format("%0" + paddingLength + "d", 0) + hex;
		} else {
			return hex;
		}
	}

	public static boolean validatePassword(String originalPassword, String storedPassword){
		String[] parts = storedPassword.split(":");
		int iterations = Integer.parseInt(parts[0]);
		byte[] salt = fromHex(parts[1]);
		byte[] hash = fromHex(parts[2]);
		char[] chars = originalPassword.toCharArray();
		boolean valid = false;

		PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, hash.length * 8);
		Arrays.fill(chars, Character.MIN_VALUE);
		
		SecretKeyFactory skf;
		try {
			skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
			byte[] testHash = skf.generateSecret(spec).getEncoded();
			int diff = hash.length ^ testHash.length;
			for (int i = 0; i < hash.length && i < testHash.length; i++) {
				diff |= hash[i] ^ testHash[i];
			}
			valid = diff == 0;
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Algoritmo n�o encontrado!");
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			System.err.println("Erro no processo de Hash!");
			e.printStackTrace();
		} finally {
			spec.clearPassword();
		}
		
		return valid;
	}

	public static byte[] fromHex(String hex){
		byte[] bytes = new byte[hex.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}

}