package com.senseplus.security;
import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.Date;   
import io.jsonwebtoken.Claims;
 
public class JWTManager {

	private static Key signingKey = new SecretKeySpec(Keys.secretKeyFor(SignatureAlgorithm.HS256).getEncoded(), SignatureAlgorithm.HS256.getJcaName());
	//Sample method to validate and read the JWT
	public static Claims parseJWT(String jwt) {
		
		
	    //This line will throw an exception if it is not a signed JWS (as expected)
	    Claims claims = Jwts.parser()         
	       .setSigningKey(signingKey)
	       .parseClaimsJws(jwt).getBody();
	    return claims;
//	    System.out.println("ID: " + claims.getId());
//	    System.out.println("Subject: " + claims.getSubject());
//	    System.out.println("Issuer: " + claims.getIssuer());
//	    System.out.println("Expiration: " + claims.getExpiration());
	}
	
	public static String createJWT(String id, String issuer, String subject, long ttlMillis) {
		 
	    //The JWT signature algorithm we will be using to sign the token
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	 
	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
	 
	    //We will sign our JWT with our ApiKey secret
	   
	 
	    //Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder().setId(id)
	                                .setIssuedAt(now)
	                                .setSubject(subject)
	                                .setIssuer(issuer)
	                                .signWith(signingKey, signatureAlgorithm);
	 
	    //if it has been specified, let's add the expiration
	    if (ttlMillis >= 0) {
	    long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }
	 
	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();
	}
}
