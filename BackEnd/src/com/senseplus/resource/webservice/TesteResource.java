package com.senseplus.resource.webservice;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.ibatis.exceptions.PersistenceException;

import com.senseplus.dao.DAO;
import com.senseplus.persistence.entities.Empresa;
import com.senseplus.persistence.entities.Papel;
import com.senseplus.persistence.entities.PapelUsuario;
import com.senseplus.persistence.entities.Usuario;
import com.senseplus.security.JWTManager;
import com.senseplus.security.PasswordEncryptor;

import io.jsonwebtoken.Claims;

@Path("/teste")
public class TesteResource {

	@GET
	@Path("/usuario")
	@Produces(MediaType.APPLICATION_JSON)
	public Response usuario(@Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);

		Usuario user = DAO.USUARIO.loadFull(Integer.parseInt(tokenInfo.getId()));

		return Response.ok().entity(user).build();
	}

	@GET
	@Path("/usuarios")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Usuario> usuarios(@Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);

		if (tokenInfo.getSubject().contains("1")) {
			return DAO.USUARIO.findAll(null);
		}
		if(tokenInfo.getSubject().contains("2")) {
			return DAO.USUARIO.findAll(Integer.parseInt(tokenInfo.getIssuer()));
		}
		
		
		return null;

	}

	@POST
	@Path("/usuario/alterar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response alteracaoUsuario(Usuario usuario, @Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);

		Usuario usuarioAlteracao = new Usuario();
		usuarioAlteracao.setId(Integer.parseInt(tokenInfo.getId()));
		usuario.setUsuarioUltimoAlteracao(usuarioAlteracao);

		DAO.USUARIO.update(usuario);

		return Response.ok().build();
	}
	
	@POST
	@Path("/usuario/criar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response criarUsuario(Usuario usuario, @Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);

		Usuario usuarioAlteracao = new Usuario();
		usuarioAlteracao.setId(Integer.parseInt(tokenInfo.getId()));
		usuario.setUsuarioUltimoAlteracao(usuarioAlteracao);
		Empresa empresa = new Empresa();
		empresa.setId(Integer.valueOf(tokenInfo.getIssuer()));
		usuario.setEmpresa(empresa);

		DAO.USUARIO.insert(usuario);

		return Response.ok().build();
	}

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(Usuario usuario) {
		try {
			Usuario banco = DAO.USUARIO.auth(usuario.getLogin());
			if (PasswordEncryptor.validatePassword(usuario.getSenha(), banco.getSenha())) {
				
				//id do token -> id_usuario
				//issuer->id_empresa
				//subject->id_papel []
					
				String token = JWTManager.createJWT(banco.getId().toString(), "1", banco.getPapeis().toString(),
						20 * 60 * 10000L);
				return Response.ok().entity(token).build();
			}
		} catch (PersistenceException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(Status.UNAUTHORIZED).build();
	}

	@POST
	@Path("/papel/alterar")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response alteracaoPapel(Usuario usuario, @Context HttpHeaders headers) {
		
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);
		System.out.println(tokenInfo.getSubject());
		Papel master = new Papel();
		master.setNome("Master");
		
		if ((tokenInfo.getSubject().contains("2")&& !usuario.getPapeis().contains(master)) || tokenInfo.getSubject().contains("1")) {
			
			DAO.PAPEL_USUARIO.delete(usuario.getId());
			
			List<PapelUsuario> list = usuario.getPapeis().stream().map(papel->new PapelUsuario(papel, usuario)).collect(Collectors.toList());
			
			DAO.PAPEL_USUARIO.insertBatch(list);
		}
		else {
			
			return Response.status(Status.UNAUTHORIZED).build();
			
		}
		return Response.ok().build();
	}
	
	@GET
	@Path("/permissao/master")
	@Produces(MediaType.APPLICATION_JSON)
	public Response master(@Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);

		if(tokenInfo.getSubject().contains("1")) {
			return Response.ok().build();
		}
		else
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}

		
	}
	@GET
	@Path("/permissao/admin")
	@Produces(MediaType.APPLICATION_JSON)
	public Response admin(@Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);
		if(tokenInfo.getSubject().contains("2")) {
			return Response.ok().build();
		}
		else
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}
	}
	@GET
	@Path("/permissao/instrutor")
	@Produces(MediaType.APPLICATION_JSON)
	public Response instrutor(@Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);
		if(tokenInfo.getSubject().contains("3")) {
			return Response.ok().build();
		}
		else
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}
	}
	@GET
	@Path("/permissao/aluno")
	@Produces(MediaType.APPLICATION_JSON)
	public Response aluno(@Context HttpHeaders headers) {
		String token = headers.getHeaderString("Authorization");
		token = token.replace("Bearer ", "");
		Claims tokenInfo = JWTManager.parseJWT(token);
		
		if(tokenInfo.getSubject().contains("4")) {
			return Response.ok().build();
		}
		else
		{
			return Response.status(Status.UNAUTHORIZED).build();
		}
	}
}
